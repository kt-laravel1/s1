<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;

use Illuminate\Http\Request;

class PostController extends Controller
{
    public function create(){
        return view('post.create');
    }

    public function store(Request $request)
    {
        if(Auth::user()){
            $post = new Post;

            $post->title = $request->input('title');
            $post->content = $request->input('content');

            $post->user_id = (Auth::user()->id);
            $post->save();

            return redirect('/posts');
        }else{
            return redirect('/login');
        }
    }

    public function index(){
        $posts = Post::all();
        return view('post.index')->with('posts', $posts);
    }

    public function myPost()
    {
        if(Auth::user()){
            $posts = Auth::user()->posts;
            return view('post.index')->with('post', $posts);
        }else{
            return redirect('/login');
        }
    }

    public function show($id)
    {
        $post = Post::find($id);
        return view('post.show')->with('post', $post);
    }

    public function edit($id){
        $post = Post::find($id);
        return view('post.edit')->with('post', $post);
    }

    public function update(Request $request, $id)
    {
        $post = Post::find($id);
        //if authenticated user's ID is the same as the post's user_id
        if (Auth::user()->id == $post->user_id) {
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->save();
        }
        return redirect('/posts');
    }

    public function destroy($id)
    {
        $post = Post::find($id);
        //if authenticated user's ID is the same as the post's user_id
        if (Auth::user()->id == $post->user_id) {
            $post->delete();
        }
        return redirect('/posts');
    }

    public function like($id)
    {
        $post = Post::find($id);
        $user_id = Auth::user()->id;
        //if authenticated user is not the post author
        if ($post->user_id != $user_id) {
            //check if a post like has been made by this user before
            if ($post->likes->contains("user_id", $user_id)) {
                //delete the like made by this user to unlike this post
                PostLike::where('post_id', $post->id)->where('user_id', $user_id)->delete();
            } else {
                //create a new like record to like this post
                //instantiate a new PostLike object from the PostLike model
                $postLike = new PostLike;
                //define the properties of the $postLike object
                $postLike->post_id = $post->id;
                $postLike->user_id = $user_id;

                //save this postLike object in the database
                $postLike->save();
            }
            return redirect("/posts/$id");
        }
    }

    public function archive($id)
    {
        $post = Post::find($id);
        //if authenticated user's ID is the same as the post's user_id
        if (Auth::user()->id == $post->user_id) {
            $post->isActive = false;
            $post->save();
        }
        return redirect('/posts');
    }

    public function comment(Request $request, $id)
    {
        $post = Post::find($id);
        $user_id = Auth::user()->id;

        $postComment = new PostComment;
        $postComment->post_id = $post->id;
        $postComment->user_id = $user_id;
        $postComment->content = $request->input('content');
        $postComment->save();

        return redirect("/posts/$id");
    }


}