@extends('layouts.app')
@section('content')
    <form action="/posts" method="POST">
        @csrf

        <div class="container w-75 p-3 border">
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control" id="title" name="title">
            </div>
            <div class="form-group">
                <label for="content">Content</label>
                <textarea name="content" id="content" rows="3" class="form-control"></textarea>
            </div>
            <div class="mt-4">
                <button class="btn btn-primary w-100">Create Post</button>
            </div>
        </div>


    </form>
@endsection
