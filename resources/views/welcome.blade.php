@extends('layouts.app')
@section('content')
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Laravel Blog</title>
    </head>

    <body>


        <div class="container bordered text-center">
            <h1 class="display-5">WELCOME TO LARAVEL BLOG!</h1>
            <h3>Share your stories across the globe</h3>

            <div>
                <img style="border-radius: 25px" src="https://acegif.com/wp-content/uploads/2021/4fh5wi/welcome-15.gif"
                    class="img-fluid my-2 shadow" alt="">
            </div>

        </div>

    </body>

    </html>
@endsection
